<?php
$context = null;
$idCPF = null;
$data = array(
    "creationDate" => "2019-03-10",
    "CPF" => "10041391950",
    "RG" => "429434121",
    "firstName" => "Carlos",
    "lastName" => "Rocha",
    "type" => "O+",
    "telephone" => "33282960",
    "allergy" => array(
        "allergy1" => "lactose",
    ),
    "deficiency" => array(
        "deficiency1" => "braço esquerdo amputado"
    ),
    "medicalInfo"=> array(
        "nameMedic" => "Rogério Cardoso",
        "CRM" => "033P",
        "city" => "Blumenau",
        "hospitalName" => "Santa Catarina",
        "exams" => array(
            "examType" => "Hemograma",
            "results" => "VSH superior à 15 mm/h",
            "lab" => "Hemos",
            "lastCRM" => "116P",
            "lastMedic" => "Mário Reich",
            "lastCity" => "Blumenau",
            "lastHospital" => "Santo Antônio"
    )
  )
);
?>
<!DOCTYPE html>
<html>
<head>
    <style>
    * {
      margin-left: 10px;
      margin-right: 30px;
    }
    </style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Castor</title>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
</head>
<body>

<div class="wrapper">
  <h2>Busca de Clientes </h2><pre size="10" class="alert alert-info" role="alert">utilizar somente números na busca no CPF</pre><br />
  <form method="post"  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">#</span>
      <input name="idCPF" type="number" class="form-control" placeholder="CPF do paciente" aria-describedby="basic-addon1" maxlength="11">
  </div><br />

  <input name="Submit" type="submit" value="Buscar" class="btn btn-info" style="margin-left: 10px;">
  <input name="Limpar" type="submit" value="Limpar" class="btn btn-info" style="margin-left: 5px;">
 </form>
<br />
</div>

<?php
    if(isset($_POST['Limpar'])) {
        unset($_POST);
        header("Refresh:0");
    }

    if(isset($_POST['Submit'])) {
      $context = true;
      if($context === true) {
        $idCPF = $_POST['idCPF'];
        if(empty($idCPF)) {
          echo "<h3 class='alert alert-danger' role='alert'>CPF não informado!</h3>";
          exit();
        }
        if($idCPF === '10041391950'){

          echo '<table>';
          echo '<p><pre size="12" style="margin-left: 5px; margin-right:500px;">NOME '; print_r($data['firstName'] . ' ' . $data['lastName']); echo '   TELEFONE '; print_r($data['telephone']);
          echo '</pre>';
          echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">CPF  '; print_r($data['CPF']);echo  '    RG '  ; print_r($data['RG']);
          echo '</pre>';
          echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">TIPO SANGUÍNEO '; print_r($data['type']);
          echo '    ALERGIAS ';
          if(sizeof($data['allergy']) >= 2) {
            foreach ($data['allergy'] as $info) {
                print_r($info . ', ');
            }
          } else {
            print_r($data['allergy']['allergy1']);
            }

          echo '</pre>';
          echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">DEFICIÊNCIA ';
            foreach ($data['deficiency'] as $info){
              if(sizeof($data['deficiency']) >= 2) {
                print_r($info . ', ');
              } else {
                print_r($data['deficiency']['deficiency1']);
              }
            }
          echo '</pre></p></table>';
          //MEDICO
          echo '<div data-role="collapsible-set">';
          echo '<div data-role="collapsible" data-collapsed="true">';
          echo '<h3>MÉDICO</h3>';
          echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">NOME  '; print_r($data['medicalInfo']['nameMedic']); echo '  CRM ';
          print_r($data['medicalInfo']['CRM']); echo'</pre>'; echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">CIDADE  '; print_r($data['medicalInfo']['city']);
          echo '   HOSPITAL '; print_r($data['medicalInfo']['hospitalName']);echo'</pre>';
          echo '</div>';
          echo '</div>';
          //EXAMES
          echo '<div data-role="collapsible-set">';
          echo '<div data-role="collapsible" data-collapsed="true">';
          echo '<h3>EXAMES</h3>';
          echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">TIPO '; print_r($data['medicalInfo']['exams']['examType']); echo '        RESULTADOS '; print_r($data['medicalInfo']['exams']['results']);
          echo '</pre>';
          echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">LABORATÓRIO '; print_r($data['medicalInfo']['exams']['lab']); echo '     CRM '; print_r($data['medicalInfo']['exams']['lastCRM']);
          echo '</pre>';
          echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">MÉDICO '; print_r($data['medicalInfo']['exams']['lastMedic']); echo '    CIDADE '; print_r($data['medicalInfo']['exams']['lastCity']);
          echo '</pre>';
          echo '<pre size="12" style="margin-left: 5px; margin-right:500px;">HOSPITAL '; print_r($data['medicalInfo']['exams']['lastHospital']);
          echo '</pre>';
          echo '</div>';
          echo '</div>';
        } else
          echo '<h3 class="alert alert-danger" role="alert">Paciente não encontrado!</h3>';
          exit();
      }
    }
?>

</body>
</html>
